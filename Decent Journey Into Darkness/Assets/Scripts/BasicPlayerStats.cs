﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicPlayerStats : MonoBehaviour
{
    public int moveDist;
    [SerializeField]
    int moveDistanceModifier;
    int healthPoints = 2;
    int armour = 0;
    public int armourModifier = 0;
    //public int movementModifier;
    bool isStunned = false;
    int tileX;
    int tileY;

    public int MoveDistModifier
    {
        get
        {
            return moveDistanceModifier;
        }
        set
        {
            moveDistanceModifier += value;
            if (moveDistanceModifier <= 0 && isStunned == false)
            {
                Debug.Log("The player can't have less than 0 movement");
                moveDistanceModifier = 1;
            }
        }
    }

    public int HealthPoints
    {
        get
        {
            return healthPoints;
        }
    }

    public int Armour
    {
        get
        {
            return armour;
        }

        set
        {
            armour = value;
        }
    }

    private void Start()
    {
        moveDistanceModifier = moveDist;
    }

    private void Update()
    {

    }

}
