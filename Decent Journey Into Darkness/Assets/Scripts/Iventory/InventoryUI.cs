﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    PlayerChangeOver playerChanged;
    List<ArmourLoot> armourLoot = new List<ArmourLoot>();
    // FIX ME: Inventory slots will have to change depending on current player
    public List<InventorySlotUI> inventorySlots;
    GameObject currentPlayer;

    // Use this for initialization
    void Start()
    {
        playerChanged = PlayerChangeOver.instance;
        currentPlayer = playerChanged.currentPLayer;
        playerChanged.endTurnCallback += ChangeCurrentPlayer;
        playerChanged.endTurnCallback += UpdateUI;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateUI();
    }

    public void UpdateUI()
    {
        armourLoot = currentPlayer.GetComponent<Inventory>().Armour;
        for (int i = 0; i < armourLoot.Count; i++)
        {
            inventorySlots[i].itemAttributesText.text = "Armour Value: " + armourLoot[i].ArmourMod.ToString() + "\nMovement: " + armourLoot[i].MoveDistMod.ToString();
            inventorySlots[i].armourMod = armourLoot[i].ArmourMod;
            inventorySlots[i].moveDistMod = armourLoot[i].MoveDistMod;
        }
    }
    private void ChangeCurrentPlayer()
    {
        currentPlayer = playerChanged.currentPLayer;
    }
}
