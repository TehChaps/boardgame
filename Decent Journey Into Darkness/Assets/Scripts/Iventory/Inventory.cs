﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{

    List<ArmourLoot> armour = new List<ArmourLoot>();
    List<WeaponLoot> weapons = new List<WeaponLoot>();

    public List<ArmourLoot> Armour
    {
        get
        {
            return armour;
        }

        set
        {
            armour = value;
        }
    }

    BasicPlayerStats playerStats;

    InventorySlotUI equipedItem;

    [SerializeField]
    GameObject equipedInventSlot1;
    [SerializeField]
    GameObject equipedInventSlot2;
    [SerializeField]
    GameObject equipedInventSlot3;

    public List<GameObject> inventoryItems = new List<GameObject>();
    public List<GameObject> equipedItems = new List<GameObject>();

    public float speed = 1.0F;
    private float startTime;
    float journeyLength;
    PlayerChangeOver currentPlayer;

    // more lists for apparel and magic items;

    // Use this for initialization
    void Start()
    {
        currentPlayer = PlayerChangeOver.instance;
        playerStats = currentPlayer.currentPLayer.GetComponent<BasicPlayerStats>();
        currentPlayer.endTurnCallback += ChangeCurrentPLayer;
        //playerStats = this.gameObject.GetComponent<BasicPlayerStats>();
        //journeyLength = Vector3.Distance(equipedItem.transform.position, equipedInventSlot2.transform.position);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Equip(InventorySlotUI inventorySlot)
    {
        if (equipedItem == null)
        {
            equipedItem = inventorySlot;
            inventoryItems.Remove(inventorySlot.gameObject);
            equipedItems.Add(equipedItem.gameObject);
            playerStats.armourModifier = inventorySlot.armourMod;
            playerStats.MoveDistModifier = inventorySlot.moveDistMod;
            Debug.Log(playerStats.MoveDistModifier);
            //inventorySlot.transform.position = Vector3.Lerp(inventorySlot.gameObject.transform.position, equipedInventSlot2.transform.position, 2f);
            inventorySlot.transform.position = equipedInventSlot2.transform.position;
        }
        else
            Debug.Log("You Already Have An ITEM Equiped");
    }


    public void UnEquip()
    {

    }

    private void ChangeCurrentPLayer()
    {
        playerStats = currentPlayer.currentPLayer.GetComponent<BasicPlayerStats>();
    }

}
