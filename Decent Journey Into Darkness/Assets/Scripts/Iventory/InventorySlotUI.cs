﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlotUI : InventoryUI
{
    Inventory playerInvent;
    public Text armourName;
    public Text itemAttributesText;
    public GameObject currentPlayer;

    public int armourMod;
    public int moveDistMod;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
}
