﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    public int worldWidth = 10;
    public int worldHeight = 10;

    public GameObject tile;

    GameObject tileParent;
    GameObject[,] tiles;

    public int WorldWidth
    {
        get
        {
            return worldWidth;
        }
    }

    public int WorldHeight
    {
        get
        {
            return worldHeight;
        }
    }

    // Use this for initialization
    void Start()
    {
        tiles = new GameObject[WorldWidth, WorldHeight];
        tileParent = new GameObject();
        tileParent.name = "tileParent";
        InstantiateWorld();
    }

    // Update is called once per frame
    void Update()
    {
    }

    /// <summary>
    /// InstantiateWorld is a Placeholder function it wont be in the final game.
    /// </summary>
    private void InstantiateWorld()
    {
        for (int x = 0; x < WorldWidth; x++)
        {
            for (int y = 0; y < WorldHeight; y++)
            {
                GameObject tileGO = Instantiate<GameObject>(tile, new Vector3(x, 0, y), Quaternion.identity);
                tileGO.name = string.Format("Tile: {0},{1}", x, y);
                tileGO.transform.parent = tileParent.transform;
                tiles[x, y] = tileGO;
            }
        }
    }
    /// <summary>
    /// GetTilesAt function finds a tile on the map taking in a coord and returns a gameobject.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public GameObject GetTileAt(int x, int y)
    {
        if (x > -1 && y > -1 && x < WorldWidth && y < WorldHeight)
            return tiles[x, y];
        else
            return null;
    }
}
