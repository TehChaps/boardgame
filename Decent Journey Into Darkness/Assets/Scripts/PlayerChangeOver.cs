﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerChangeOver : MonoBehaviour
{

    #region Singleton
    public static PlayerChangeOver instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public delegate void EndTurnFunction();
    public EndTurnFunction endTurnCallback;

    // FIX ME: Need a function to get the current player. Use a list of players?
    public GameObject currentPLayer;

    public List<GameObject> players;
    int listIndex = 0;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void EndTurn()
    {
        ChangeCurrentPlayer();
        endTurnCallback.Invoke();
    }
    private void ChangeCurrentPlayer()
    {
        if (listIndex < players.Count-1)
        {
            listIndex++;
            currentPLayer = players[listIndex];
        }
        else
        {
            listIndex = 0;
            currentPLayer = players[listIndex];
        }
    }

}
