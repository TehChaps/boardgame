﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effects
{
    int armourEffects;
    int moveDistEffects;
    int healthpointEffetcts;

    public Effects(int armour,int moveDist,int hp)
    {
        armourEffects = armour;
        moveDistEffects = moveDist;
        healthpointEffetcts = hp;
    }
}
