﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceManagement : MonoBehaviour {

    bool isRollReady = true;
    public Transform[] dice = new Transform[7]; //1st = black // 2nd = blue // 3rd = brown // 4th = green // 5th = grey // 6th = red // 7th = yellow // 
    public GameObject[] diceSpawnPoints = new GameObject[5];
    public ForceMode forceMode;
    public float forceMul = 100.0f;
    public float touqueMul = 100.0f;
    public Transform diceAim;


    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (isRollReady)
        {
            if (Input.GetKeyUp(KeyCode.Space)) //press space to roll dice
            {
                RollDice(Random.Range(1, 5), Random.Range(1, 7));
            } 
        }
    }

    public void RollDice(int amountOfDice, int dieIndex)
    {
        for (int i = amountOfDice; i != 0; i--)
        {
            Transform newDie = Instantiate(dice[dieIndex]);
            newDie.position = diceSpawnPoints[i].transform.position;
            newDie.rotation = diceSpawnPoints[i].transform.rotation;
            newDie.GetComponent<Rigidbody>().AddForce(Random.onUnitSphere * forceMul, forceMode);
            newDie.GetComponent<Rigidbody>().AddTorque(Random.onUnitSphere * touqueMul, forceMode);
            newDie.GetComponent<Rigidbody>().AddForce(Vector3.up * 100, forceMode);
            newDie.GetComponent<DiceBehaviour>().diceAimPoint = diceAim;
            newDie.name = "Dice" + i;
        }
    }
}
