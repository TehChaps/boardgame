﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceRollingScript : MonoBehaviour
{

    public Rigidbody diceRB;
    public bool isRollReady = true;
    public ForceMode forceMode;
    public float forceMul = 10.0f;
    public float touqueMul = 10.0f;
    public Transform diceAimPoint;
    public bool isGrounded;
    public float lerpSpeed;

    // Use this for initialization
    void Start ()
    {
        diceRB = gameObject.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (isRollReady)
        {
            if (Input.GetKeyUp(KeyCode.Space))
            {
                RollDice();
            }
        }

        if (!isGrounded)
        {
            transform.position = Vector3.Lerp(transform.position, diceAimPoint.position, lerpSpeed);
        }
	}

    public void RollDice()
    {
        diceRB.AddForce(Random.onUnitSphere * forceMul, forceMode);
        diceRB.AddTorque(Random.onUnitSphere * touqueMul, forceMode);
        diceRB.AddForce(Vector3.up * 10, forceMode);
    }



    public void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "DiceBoxFloor")
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
    }

    public void OnCollisionExit(Collision collision)
    {
        isGrounded = false;
    }
}
