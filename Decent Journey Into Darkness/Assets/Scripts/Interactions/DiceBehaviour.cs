﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceBehaviour : MonoBehaviour {

    public bool isGrounded = false;
    public Transform diceAimPoint;
    public float lerpSpeed;
    public float timer = 1.0f;
    public int bounces = 2;
    public float forceMul = 100.0f;
    public float touqueMul = 100.0f;
    public ForceMode forceMode;
    public Rigidbody diceRB;

    // Use this for initialization
    void Start ()
    {
        diceRB = gameObject.GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update ()
    {
		 if (isGrounded == false)
         {
            transform.position = Vector3.Lerp(transform.position, diceAimPoint.position, lerpSpeed);
            
         }
         if (bounces <= 0)
         {
            isGrounded = true;
         }

         //timer for how long the die wiggles creates a tone of ramdomisation
        /* timer -= Time.deltaTime;

         if (timer > 0)
         {
             isGrounded = false;
         }
         if (timer < 0)
         {
             isGrounded = true;
         }*/

        //LerpTimer(1);

    }

    public void LerpTimer(float timer)
    {

        timer -= Time.deltaTime;

        if (timer > 0)
        {
            isGrounded = false;
        }
        if (timer < 0)
        {
            isGrounded = true;

        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        int inheritBounceForce = 30;
        if (collision.transform.tag == "DiceBoxFloor")
        {
            if (bounces > 0)
            {
                BouceDie(inheritBounceForce);
                bounces = bounces - 1;
                inheritBounceForce = inheritBounceForce - 20;
            }
        }
        Debug.Log("Hit");
    }

    public void OnCollisionStay(Collision collision)
    {
        
    }

    public void BouceDie(int FusRoDah)
    {
        diceRB.AddForce(Random.onUnitSphere * forceMul, forceMode);
        diceRB.AddTorque(Random.onUnitSphere * touqueMul, forceMode);
        diceRB.AddForce(Vector3.up * FusRoDah, forceMode);
        Debug.Log("boing");
    }

}