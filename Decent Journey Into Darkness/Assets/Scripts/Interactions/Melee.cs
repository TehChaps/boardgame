﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee : MonoBehaviour
{

    DiceManagement dice = new DiceManagement();

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Attack(GameObject enemy)
    {
        EnemyDeath(enemy);
    }
    public void EnemyDeath(GameObject enemy)
    {
        Destroy(enemy);
    }

}
