﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerMovement : MonoBehaviour
{

    #region Singleton
    public static PlayerMovement instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public GameObject cubeGO;
    public GameObject movePreview;
    public MeshRenderer movePreviewMeshRenderer;

    int playermoveDist;
    int mapLayer;
    int enemyLayer;

    GameObject[,] oldTilesMovePreview;
    GameObject[,] movementTiles;
    GameObject[,] arrayRefresh;
    public Material tileMat;
    Material ogTileMat;

    WorldCreation world;
    PlayerChangeOver currentPlayer;

    public List<WorldCreation.Tile> currentPath = null;

    public float speed;
    private float startTime = 0;
    float journeyLength;

    int currentTile;
    private bool startTimer = false;
    public float baseMoveTime = 1f;
    private float moveTimer;
    int n = 0;

    Melee meleeAttack = new Melee();

    // Use this for initialization
    void Start()
    {
        world = GameObject.Find("PlayAreaManager").GetComponent<WorldCreation>();
        movePreviewMeshRenderer = movePreview.GetComponentInChildren<MeshRenderer>();
        ogTileMat = cubeGO.GetComponentInChildren<MeshRenderer>().material;
        oldTilesMovePreview = new GameObject[world.WorldWidth, world.WorldHeight];
        arrayRefresh = new GameObject[world.WorldWidth, world.WorldHeight];
        movementTiles = new GameObject[playermoveDist, playermoveDist];
        mapLayer = LayerMask.GetMask("Map");
        enemyLayer = LayerMask.GetMask("Enemy");
        currentPlayer = PlayerChangeOver.instance;
        playermoveDist = currentPlayer.currentPLayer.GetComponent<BasicPlayerStats>().moveDist;
        cubeGO = currentPlayer.currentPLayer;
        currentPlayer.endTurnCallback += OnPlayerChanged;
        //ShowMovementPossibilities();
    }

    // Update is called once per frame
    void Update()
    {
        MovementPreview();
        MovePlayerTo();
        IncrementPlayerMovement();
    }

    /// <summary>
    /// PlayerMove function allows the player to click somewhere on the map and the player will move to that position on the map.
    /// </summary>
    private void PlayerMove()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ShowMovementPossibilities();
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, mapLayer))
            {
                foreach (GameObject tile in movementTiles)
                {
                    if (tile != null)
                    {
                        if (hit.transform.position == tile.transform.position)
                        {
                            cubeGO.transform.position = new Vector3(hit.transform.position.x, hit.transform.position.y + 0.5f, hit.transform.position.z);
                            movementTiles = null;
                        }
                    }
                }
            }
            //playermoveDist = cube.MoveDistModifier;
            ShowMovementPossibilities();
            // resets the array back to 0's
            Array.Clear(oldTilesMovePreview, 0, oldTilesMovePreview.Length);
        }
    }
    /// <summary>
    /// MovementPreview uses a sphere to follow the mouse and shows the player whether the player can move or not.
    /// (using the player class to get the player move distance)
    /// </summary>
    private void MovementPreview()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, mapLayer))
        {
            if (Mathf.FloorToInt(Vector3.Distance(cubeGO.transform.position, hit.transform.position)) <= playermoveDist)
            {
                movePreviewMeshRenderer.material.color = new Color(0, 255f, 0);
                movePreview.transform.position = new Vector3(hit.transform.position.x, 0, hit.transform.position.z);
            }
            else
            {
                movePreviewMeshRenderer.material.color = new Color(255f, 0, 0);
                movePreview.transform.position = new Vector3(hit.transform.position.x, 0, hit.transform.position.z);
            }
            if (cubeGO.transform.position.x != movePreview.transform.position.x && cubeGO.transform.position.z != movePreview.transform.position.z)
            {
                Quaternion lookRot = Quaternion.LookRotation(cubeGO.transform.position - movePreview.transform.position, Vector3.up);
                cubeGO.transform.rotation = Quaternion.Slerp(cubeGO.transform.rotation, lookRot, 4 * Time.deltaTime);
            }
        }
        else
            return;
    }

    /// <summary>
    /// ShowMovementPossibilities changes the tiles around the player depending on how far they can move.
    /// the tiles change colour.
    /// </summary>
    private void ShowMovementPossibilities()
    {
        foreach (GameObject oldTile in oldTilesMovePreview)
        {
            if (oldTile != null)
                oldTile.GetComponentInChildren<MeshRenderer>().material = ogTileMat;
        }

        int radius = playermoveDist;
        for (int x = (int)cubeGO.transform.position.x - radius; x <= (int)cubeGO.transform.position.x + radius; x++)
        {
            for (int y = (int)cubeGO.transform.position.z - radius; y <= (int)cubeGO.transform.position.z + radius; y++)
            {
                GameObject moveTile = world.GetTileAt(x, y);

                if (moveTile == null)
                {

                }
                else
                {
                    moveTile.GetComponentInChildren<MeshRenderer>().material = tileMat;
                    oldTilesMovePreview[x, y] = moveTile;
                    Material oldTileMat = moveTile.GetComponentInChildren<MeshRenderer>().material;
                }
            }
        }
        movementTiles = oldTilesMovePreview;
    }


    private void MovePlayerTo()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "Enemy")
                {
                    if (Vector3.Distance(cubeGO.transform.position, hit.transform.position) <= 1.5f)
                        meleeAttack.Attack(hit.transform.gameObject);
                    return;
                }
                if (hit.collider.tag == "Chest")
                {
                    return;
                }
            }
            if (Physics.Raycast(ray, out hit, mapLayer))
            {
                if (hit.transform.position != null)
                {
                    world.MovePlayerTo((int)hit.transform.position.x, (int)hit.transform.position.z);
                    MovePlayerToTile();
                }
            }
        }
        if (currentPath != null)
        {
            int currentTile = 0;
            while (currentTile < currentPath.Count - 1)
            {
                Vector3 start = new Vector3((int)currentPath[currentTile].x, 0, (int)currentPath[currentTile].y) + new Vector3(0, 1, 0);
                Vector3 end = new Vector3((int)currentPath[currentTile + 1].x, 0, (int)currentPath[currentTile + 1].y) + new Vector3(0, 1, 0); ;
                Debug.DrawLine(start, end, Color.red);
                currentTile++;
            }
        }
    }

    public void MovePlayerToTile()
    {
        //Debug.Log(currentPath.Count);        
        if (currentPath == null)
        {
            return;
        }

        startTimer = true;
    }

    private void IncrementPlayerMovement()
    {
        if (startTimer == true)
        {
            moveTimer += Time.deltaTime;

            if (moveTimer >= baseMoveTime)
            {
                if (currentPath.Count >= playermoveDist)
                {
                    if (n < playermoveDist)
                    {
                        if (new Vector3((float)currentPath[n].x, 0, (float)currentPath[n].y) != null)
                            cubeGO.transform.position = new Vector3((float)currentPath[n].x, 0, (float)currentPath[n].y);
                        else
                            return;
                        moveTimer = 0;
                        n++;
                    }
                    else if (n >= playermoveDist)
                    {
                        currentPath = null;
                        n = 0;
                        startTimer = false;
                    }
                }
                else if (currentPath.Count < playermoveDist)
                {
                    if (n < currentPath.Count)
                    {
                        if (new Vector3((float)currentPath[n].x, 0, (float)currentPath[n].y) != null)
                            cubeGO.transform.position = new Vector3((float)currentPath[n].x, 0, (float)currentPath[n].y);
                        else
                            return;

                        Debug.Log("Timer Stopped");
                        moveTimer = 0;
                        n++;
                        if (n == currentPath.Count)
                        {
                            n = 0;
                            startTimer = false;
                            currentPath = null;
                        }

                    }
                }
            }
        }
    }

    private void OnPlayerChanged()
    {
        cubeGO = currentPlayer.currentPLayer;
        playermoveDist = cubeGO.GetComponent<BasicPlayerStats>().MoveDistModifier;
        Debug.Log(cubeGO.GetComponent<BasicPlayerStats>().MoveDistModifier);
    }
}
