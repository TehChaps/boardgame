﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapTilesLerp : MonoBehaviour
{
    [SerializeField]
    private float startAnimTime;
    public float speed = 1.0F;
    private float startTime;
    float journeyLength;
    [SerializeField]
    private GameObject parent;

    // Use this for initialization
    void Start()
    {
        startTime = startAnimTime;
        journeyLength = Vector3.Distance(transform.position, new Vector3(transform.position.x, -.5f, transform.position.z));
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position != new Vector3(transform.position.x, -.5f, transform.position.z) && Time.time >= startAnimTime)
        {
            MapPieceAnim();
        }
        else if (transform.position == new Vector3(transform.position.x, -.5f, transform.position.z))
        {
            /*for (int i = 0; i < this.transform.childCount; i++)
            {
                this.transform.GetChild(i).parent = parent.transform;
            }
            this.gameObject.SetActive(false);*/
            this.gameObject.GetComponent<MapTilesLerp>().enabled = false;
            return;
        }

    }

    public void MapPieceAnim()
    {
        float distCovered = (Time.time - startTime) * speed;
        float fracJourney = distCovered / journeyLength;
        transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, -.5f, transform.position.z), fracJourney);
    }
}
