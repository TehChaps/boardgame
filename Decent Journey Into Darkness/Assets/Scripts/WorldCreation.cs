﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class WorldCreation : MonoBehaviour
{
    public List<GameObject> tiles = new List<GameObject>();
    private GameObject[,] tilesArray;
    private Tile[,] tileGraph;

    PlayerMovement player;

    private int worldWidth = 13;
    private int worldHeight = 12;

    public int WorldWidth
    {
        get
        {
            return worldWidth;
        }
    }

    public int WorldHeight
    {
        get
        {
            return worldHeight;
        }
    }

    // Use this for initialization
    void Start()
    {
        tilesArray = new GameObject[13, 12];
        for (int i = 0; i < tiles.Count; i++)
        {
            GameObject tile = tiles[i];
            tile.name = string.Format("Tile: {0} {1}", (int)tile.transform.position.x, (int)tile.transform.position.z);
            tilesArray[(int)tile.transform.position.x, (int)tile.transform.position.z] = tile.gameObject;
        }
        player = PlayerMovement.instance;
        GeneratePathFindingGraph();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public GameObject GetTileAt(int x, int y)
    {
        if (x > -1 && y > -1 && x < WorldWidth && y < WorldHeight)
        {
            return tilesArray[x, y]; ;
        }
        else
            return null;
    }

    public class Tile
    {
        public List<Tile> neighbours;
        public int x;
        public int y;

        public Tile()
        {
            neighbours = new List<Tile>();
        }

        public float DistanceTo(Tile n)
        {
            return Vector3.Distance(new Vector3(x, 0, y), new Vector3(n.x, 0, n.y));
        }
    }

    void GeneratePathFindingGraph()
    {
        tileGraph = new Tile[worldWidth, worldHeight];

        #region
        // initialising the tiles in the array;
        for (int i = 0; i < tiles.Count; i++)
        {
            GameObject tile = tiles[i];
            int tilePosX = (int)tile.transform.position.x;
            int tilePosY = (int)tile.transform.position.z;
            tileGraph[tilePosX, tilePosY] = new Tile();
            tileGraph[tilePosX, tilePosY].x = tilePosX;
            tileGraph[tilePosX, tilePosY].y = tilePosY;
        }
        // calculates the neighbours of the tiles 
        for (int i = 0; i < tiles.Count; i++)
        {
            GameObject tile = tiles[i];
            int tilePosX = (int)tile.transform.position.x;
            int tilePosY = (int)tile.transform.position.z;

            if (GetTileAt(tilePosX + 1, tilePosY) != null)
            {
                tileGraph[tilePosX, tilePosY].neighbours.Add(tileGraph[tilePosX + 1, tilePosY]);
            }
            if (GetTileAt(tilePosX - 1, tilePosY) != null)
            {
                tileGraph[tilePosX, tilePosY].neighbours.Add(tileGraph[tilePosX - 1, tilePosY]);
            }
            if (GetTileAt(tilePosX, tilePosY + 1) != null)
            {
                tileGraph[tilePosX, tilePosY].neighbours.Add(tileGraph[tilePosX, tilePosY + 1]);
            }
            if (GetTileAt(tilePosX, tilePosY - 1) != null)
            {
                tileGraph[tilePosX, tilePosY].neighbours.Add(tileGraph[tilePosX, tilePosY - 1]);
            }
            if (GetTileAt(tilePosX + 1, tilePosY + 1) != null)
            {
                tileGraph[tilePosX, tilePosY].neighbours.Add(tileGraph[tilePosX + 1, tilePosY + 1]);
            }
            if (GetTileAt(tilePosX - 1, tilePosY - 1) != null)
            {
                tileGraph[tilePosX, tilePosY].neighbours.Add(tileGraph[tilePosX - 1, tilePosY - 1]);
            }
            if (GetTileAt(tilePosX - 1, tilePosY + 1) != null)
            {
                tileGraph[tilePosX, tilePosY].neighbours.Add(tileGraph[tilePosX - 1, tilePosY + 1]);
            }
            if (GetTileAt(tilePosX + 1, tilePosY - 1) != null)
            {
                tileGraph[tilePosX, tilePosY].neighbours.Add(tileGraph[tilePosX + 1, tilePosY - 1]);
            }
        }
        #endregion
    }

    public void MovePlayerTo(int x, int y)
    {
        // clear the player's path
        player.currentPath = null;
        Dictionary<Tile, float> distance = new Dictionary<Tile, float>();
        Dictionary<Tile, Tile> previous = new Dictionary<Tile, Tile>();

        List<Tile> tilesUnchecked = new List<Tile>();
        //Debug.Log((int)player.cubeGO.transform.position.x + " " + (int)player.cubeGO.transform.position.z);
        Tile source = tileGraph[(int)player.cubeGO.transform.position.x, (int)player.cubeGO.transform.position.z];
        Tile target = tileGraph[x, y];
        distance[source] = 0;
        previous[source] = null;

        foreach (Tile tile in tileGraph)
        {
            if (tile != null)
            {
                if (tile != source)
                {
                    distance[tile] = Mathf.Infinity;
                    previous[tile] = null;
                }
                tilesUnchecked.Add(tile);
            }
        }

        while (tilesUnchecked.Count > 0)
        {
            // u is the unchecked tile with the smallest distance.
            Tile u = null;
            foreach (Tile possibleU in tilesUnchecked)
            {
                if (u == null || distance[possibleU] < distance[u])
                {
                    u = possibleU;
                }
            }

            if (u == target)
            {
                break;
            }
            tilesUnchecked.Remove(u);

            foreach (Tile v in u.neighbours)
            {
                float alt = distance[u] + u.DistanceTo(v);
                if (alt < distance[v])
                {
                    distance[v] = alt;
                    previous[v] = u;
                }
            }
        }

        if (previous[target] == null)
        {
            // no route between the player and the target
            return;
        }

        List<Tile> currentPath = new List<Tile>();
        Tile currentTile = target;
        while (previous[currentTile] != null)
        {
            currentPath.Add(currentTile);
            currentTile = previous[currentTile];
        }
        // at the moment the route is from the target to the source we need to reverse it.
        currentPath.Reverse();
        player.currentPath = currentPath;
    }
}
