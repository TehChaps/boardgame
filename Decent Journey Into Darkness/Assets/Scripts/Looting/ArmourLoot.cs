﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmourLoot
{
    //Effects posEffects=new Effects(0,0,0);
    //Effects negEffects;
    int armourMod;
    int moveDistMod;

    BasicPlayerStats currentPlayerStats;

    public int ArmourMod
    {
        get
        {
            return armourMod;
        }

        set
        {
            armourMod = value;
        }
    }

    public int MoveDistMod
    {
        get
        {
            return moveDistMod;
        }

        set
        {
            moveDistMod = value;
        }
    }

    public ArmourLoot(int armourMod, int moveMod)
    {
        this.ArmourMod = armourMod;
        this.MoveDistMod = moveMod;
    }

    /*public Effects PosEffects
    {
        get
        {
            return posEffects;
        }

        set
        {
            posEffects = value;
        }
    }*/
    // FIX ME: May not be needed;
    public void GetArmourEffects(GameObject currentPlayer)
    {
        currentPlayerStats = currentPlayer.GetComponent<BasicPlayerStats>();
        currentPlayerStats.armourModifier = ArmourMod;
        currentPlayerStats.MoveDistModifier = MoveDistMod;
        Debug.Log("this player can move: " + currentPlayerStats.MoveDistModifier + " and now has an armour rating of: " + currentPlayerStats.armourModifier);
    }
}
