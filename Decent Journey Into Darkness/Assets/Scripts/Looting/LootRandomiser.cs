﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootRandomiser : MonoBehaviour
{
    public List<ArmourLoot> copperArmourLoot;
    public int numOfLoots;

    // Use this for initialization
    void Start()
    {
        copperArmourLoot = new List<ArmourLoot>();
        numOfLoots = Random.Range(20, 50);
        for (int i = 0; i < numOfLoots; i++)
        {
            copperArmourLoot.Add(new ArmourLoot(Random.Range(1, 4), Random.Range(0, -3)));
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
