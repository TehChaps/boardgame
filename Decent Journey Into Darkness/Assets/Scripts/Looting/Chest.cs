﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{
    ArmourLoot armourPickup;
    LootRandomiser armourPickupList;
    PlayerChangeOver updatePlayerInventory;
    public GameObject lootCard;

    // Use this for initialization
    void Start()
    {
        updatePlayerInventory = PlayerChangeOver.instance;
        armourPickupList = GameObject.Find("PlayAreaManager").GetComponent<LootRandomiser>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OpenChest(GameObject currentPlayer)
    {
        armourPickup = armourPickupList.copperArmourLoot[Random.Range(0, armourPickupList.copperArmourLoot.Count)];
        //armourPickup.GetArmourEffects(currentPlayer);
        //Instantiate<GameObject>(lootCard);
        currentPlayer.GetComponent<Inventory>().Armour.Add(armourPickup);
        updatePlayerInventory.endTurnCallback.Invoke();
        Destroy(this.gameObject);
    }
}
