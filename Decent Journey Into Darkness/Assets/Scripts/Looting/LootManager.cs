﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootManager : MonoBehaviour
{
    Chest chest;
    Ray ray;
    int chestsLayer;

    GameObject player;
    PlayerChangeOver currentPlayer;

    /// <summary>
    /// delegates are essentialy a list of function that can get called at once might be handy for Inventory system
    /// </summary>

    // Use this for initialization
    void Start()
    {
        chestsLayer = LayerMask.GetMask("Chests");
        // FIX ME: Need a function to determine the current player.
        //player = GameObject.FindGameObjectWithTag("Player");
        currentPlayer = PlayerChangeOver.instance;
        player = currentPlayer.currentPLayer;
        currentPlayer.endTurnCallback += UpdatePlayerChanged;
    }

    // Update is called once per frame
    void Update()
    {
        PlayerOpensChest();
    }

    private void PlayerOpensChest()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit) && Vector3.Distance(player.transform.position, hit.transform.position) < 1.5f)
            {
                if (hit.collider.tag == "Chest")
                {                     
                    Debug.Log(hit.transform.name);
                    chest = hit.transform.parent.GetComponent<Chest>();
                    chest.OpenChest(player);                    
                    //onInventoryChangedCallBack.Invoke();
                    Destroy(hit.transform.parent);                                   
                }
                else
                    return;
            }
            else
            {
                return;
            }
        }
    }

    private void UpdatePlayerChanged()
    {
        player = currentPlayer.currentPLayer;
    }
}
